function PlotNewFigure(varargin)

time = varargin{3};
tmp_dat = varargin{4};
settings = varargin{5};
higher = varargin{6};
lower = varargin{7};
avg = get_avg(tmp_dat);
p_crit = 0.05;
StartTime = min(time);
StopTime = max(time);


% create array of significant values
if numel(tmp_dat) == 2
    sig_arr = NaN(1,numel(time));
    for i = 1:numel(sig_arr)
        var1 = squeeze(tmp_dat{1}(:, i, :));
        var2 = squeeze(tmp_dat{2}(:, i, :));
        if numel(var1) == numel(var2)
            [p] = signrank(var1, var2);
        else
            [p] = ranksum(var1, var2);
        end
        
        if p < p_crit
            sig_arr(1, i) = 1;
        end
    end
end

figure

for c = 1:size(avg, 1) % loop conditions
    plot(time, avg(c, :), settings.colors(c), 'LineWidth', settings.lw);
    xlim([StartTime, StopTime])
    if ~all(lower == higher, 'all')
        hold on
        % Fill Confidence intervals
        x2 = [time, fliplr(time)];
        inBetween = [higher(c, :), fliplr(lower(c, :))];
        fill(x2, inBetween, settings.colors(c))
        hold off
        alpha(.5)
    end
    hold on
end
% horizontal line
plot([min(time) max(time)],[0 0],'k')
hold on
% vertical line
plot([0 0],[-100 100],'k')
hold on 
% significance line
if numel(tmp_dat) == 2
scatter(time, sig_arr * (settings.ylim(1) + 0.5),'o',...
    'MarkerEdgeColor',[0 .5 .5], 'MarkerFaceColor',[0 .7 .7])
end
% ylabel('Amplitude [\muV]','FontSize', settings.fsz)
% xlabel('Time [s]','FontSize', settings.fsz')
title(settings.tmplabel);
xt = get(gca, 'XTick');
set(gca, 'FontSize', settings.fsz*3)
axis([StartTime StopTime settings.ylim(1), settings.ylim(2)])






end

function avg = get_avg(dat)
% Calculate average over samples.

avg = zeros(numel(dat), size(dat{1}, 1), size(dat{1}, 2));
for c = 1:numel(dat)
    avg(c, :, :) = mean(dat{c}, 3);
end
avg = squeeze(avg);
end