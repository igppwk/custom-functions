function topo_erp_error(cfg, dat, time, chanlocs, metric)
% Plots ERPs in topographical order with error bars specified in metric.
% dat = channels * pnts * samples * conditions 
% samples can be trials or subject-ERPs, conditions can be an empty
% dimension
% metric = ["SD" | "SEM"]
% cfg... settings for the plot
% cfg.condlabels e.g. ["label 1", "label 2"]
% cfg.system ["igpp", "aug"] for the acticap layout of igpp or augenklinik
% Plot Settings
settings.lw = 2;  % LineWidth
settings.fsz = 10;      % Fontsize
settings.width = 0.05;
settings.height = 0.06;
settings.colors = ['b', 'r', 'k', 'g'];
if ~isempty(cfg.condlabels)
    settings.condlabels = cfg.condlabels;
end
if ~isempty(cfg.system)
    settings.system = cfg.system;
else
    settings.system = "igpp";
end

if numel(size(dat{1})) ~= 3
    error('dat has wrong shape! Should be a 3D matrix of channels*pnts*samples')
end


%% Get error limits
[lower, higher] = get_errors(dat, metric);

% Get positions of the individual plots
positions = get_positions(settings, chanlocs);

figure;
set(gcf, 'InnerPosition',  [100, 100, 4000, 3000])
avg = get_avg(dat);


%% Set ylim
if isempty(metric)
    settings.ylim = [min(avg,[],'all')*1.2,max(avg,[],'all')*1.2 ];
else
    settings.ylim = [min(lower,[],'all')*1.2,max(higher,[],'all')*1.2 ];
end

%% Plot single plots in a loop
for e = 1:size(dat{1}, 1)
    settings.tmplabel = chanlocs(e).labels;
    tmp_dat = dat;
    for c = 1:numel(tmp_dat) % loop conditions to extract electrode of interest
        tmp_dat{c} = tmp_dat{c}(e, :, :);
    end
    singleplot(positions(e, :), time, tmp_dat, settings, higher(:, e, :), lower(:, e, :))
end

%% add legend and labels
add_legend(settings, numel(dat))
axis_labels(settings, time)

end

function [lower, higher] = get_errors(dat, metric)
% Return lower and higher error limit
nconds = numel(dat);
nbchans = size(dat{1}, 1);
pnts = size(dat{1}, 2);
% samples = size(dat{1}, 3);


higher = zeros(nconds, nbchans, pnts);
lower = zeros(nconds, nbchans, pnts);

if isempty(metric)
    return
end

for c = 1:nconds % loop conditions
    for e = 1:nbchans% loop electrodes
        for i = 1:pnts % loop time points
            tmp = dat{c}(e, i, :);
            if strcmp(metric, "SEM")
                err = std(tmp)/sqrt(length(tmp));
                higher(c, e, i) = mean(tmp) + err;
                lower(c, e, i) = mean(tmp) - err;
            elseif strcmp(metric, "SD")
                err = std(tmp);
                higher(c, e, i) = mean(tmp) + err;
                lower(c, e, i) = mean(tmp) - err;
                
            elseif strcmp(metric, "CI")
                SEM = std(tmp)/sqrt(length(tmp));
                ts = tinv([0.025  0.975],length(tmp)-1);
                higher(c, e, i) = mean(tmp)+max(ts)*SEM;
                lower(c, e, i) = mean(tmp)+min(ts)*SEM;
            end
        end
    end
end


end

function singleplot(positions, time, tmp_dat, settings, higher, lower)
StartTime = min(time);
StopTime = max(time);
avg = get_avg(tmp_dat);

axes('Position', [positions(1), positions(2), settings.width, settings.height]);
% Draw ERP
for c = 1:size(avg, 1) % loop conditions
    plot(time, avg(c, :), settings.colors(c), 'LineWidth', settings.lw);
    xlim([StartTime, StopTime])
    if ~all(lower == higher, 'all')
        hold on
        % Fill Confidence intervals
        x2 = [time, fliplr(time)];
        inBetween = [higher(c, :), fliplr(lower(c, :))];
        fill(x2, inBetween, settings.colors(c))
        hold off
        alpha(.5)
    end
    hold on
end
% horizontal line
plot([min(time) max(time)],[0 0],'k')
hold on
% vertical line
plot([0 0],[-100 100],'k')
% ylabel('Amplitude [\muV]','FontSize', settings.fsz)
% xlabel('Time [s]','FontSize', settings.fsz')
title(settings.tmplabel);
xt = get(gca, 'XTick');
set(gca, 'FontSize', settings.fsz)
axis([StartTime StopTime settings.ylim(1), settings.ylim(2)])
ax = gca;
set(ax, 'ButtonDownFcn', {'PlotNewFigure', time, tmp_dat, settings, higher, lower})

end

function axis_labels(settings, time)
StartTime = min(time);
StopTime = max(time);

axes('Position', [0.9, 0.1, settings.width, settings.height]);
plot([min(time) max(time)],[0 0],'k')
hold on
plot([0 0],[-100 100],'k')
xt = get(gca, 'XTick');
set(gca, 'FontSize', settings.fsz*2)
axis([StartTime StopTime settings.ylim(1), settings.ylim(2)])
ylabel('Amplitude [\muV]','FontSize', settings.fsz*2)
xlabel('Time [s]','FontSize', settings.fsz*2)

end

function positions = get_positions(settings, chanlocs)

positions = zeros(numel(chanlocs), 2);
Y = [chanlocs.X] + abs(min([chanlocs.X])) + 30;
Y = (Y / max(Y)) / 1.3;
X = [chanlocs.Y] * (-1);
X = X + abs(min(X)) + 45;
X = (X / max(X)) / 1.3;
Z = [chanlocs.Z] - max([chanlocs.Z]);
Z = (Z / (min(Z)*2));

if strcmp(settings.system, "igpp")
	indices_midline = [30, 21, 11, 15];
	indices_centraline = [8, 7, 22, 23];
elseif strcmp(settings.system, "aug")
    indices_midline = [5, 14, 25, 30];
    indices_centraline = [12, 13, 14, 15, 16];
end

x_range = [min(X(indices_midline)), max(X(indices_midline))];
y_range = [min(Y(indices_centraline)), max(Y(indices_centraline))];
for i = 1:numel(chanlocs) % loop electrodes
    if X(i) < x_range(1) % if electrode is on left hemisphere
        X(i) = X(i) - abs(Z(i));
    elseif X(i) > x_range(2) % if electrode is on right hemisphere
        X(i) = X(i) + abs(Z(i));
    end
    
    if Y(i) < y_range(1) % if electrode is on posterior site
        Y(i) = Y(i) - abs(Z(i));
    elseif Y(i) > y_range(2) % if electrode is on anterior site
        Y(i) = Y(i) + abs(Z(i));
    end
end

X = X + abs(min(X)) + 0.40;
X = (X / max(X)) / 1.3;
Y = Y + abs(min(Y)) + 0.40;
Y = (Y / max(Y)) / 1.3;

positions(:, 1) = X;
positions(:, 2) = Y;



end

function add_legend(settings, nconds)
% Adds a lenged for the line plots on the top right corner

if ~isempty(settings.condlabels)
    labels = settings.condlabels;
else
    labels = {};
    for c = 1:nconds
        labels{c} = sprintf("Condition %d", c);
    end
end

axes('Position', [0.9, 0.8, settings.width, settings.height]);
for c = 1:nconds
    
    plot([0 0],[-100 100],settings.colors(c))
    hold on
end

% legend(labels,'FontSize', settings.fsz*2)
[~,hObj]=legend(labels,'FontSize', settings.fsz*2);           % return the handles array
hL=findobj(hObj,'type','line');  % get the lines, not text
set(hL,'linewidth',3)            % set their width property
set(gca,'visible','off')
end

function avg = get_avg(dat)
% Calculate average over samples.

avg = zeros(numel(dat), size(dat{1}, 1), size(dat{1}, 2));
for c = 1:numel(dat)
    avg(c, :, :) = mean(dat{c}, 3);
end
avg = squeeze(avg);
end